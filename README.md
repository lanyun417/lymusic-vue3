<br/>
<br/>

<p align="center">
   <img src="./doc/logo.png" alt="logo.png" width="160" height="160" />
</p>

# <h1 align="center" style="font-size: 26px; color: #409EFF;">LYMusic 🎵</h1>

<p align="center">
   <img alt="Static Badge" src="https://img.shields.io/badge/Vue-3.3.10-%2312CC75">
   <img alt="Static Badge" src="https://img.shields.io/badge/Pinia-2.1.7-%23FFD859">
   <img alt="Static Badge" src="https://img.shields.io/badge/NodeJS-16.20.2-%230B6C09">
   <img alt="Static Badge" src="https://img.shields.io/badge/NPM-8.19.4-%23B30305">
   <img alt="Static Badge" src="https://img.shields.io/badge/Axios-1.6.2-%235226D0">
   <img alt="Static Badge" src="https://img.shields.io/badge/Version-1.0.0-%23409EFF">
</p>

<p style="font-size: 16px;" align="center">"一个适合小白、初学者学习使用的音乐网站"</p>

<p align="center"><a href="https://gitee.com/lanyun417/lymusic-vue3">项目源码 🔗</a></p>

<p align="center">💝 QQ交流群：<a href="http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=5mFNbq_kg4VW9sYIrsjubhsovn76fBMq&authKey=pSIfJpNNbUSkchH104ebtP%2BJugTD9y6%2BHkE1RV83ZWTZfhPTOIO0vR93%2BwGfDngi&noverify=0&group_code=316054036"  target="_blank">316054036</a> | 🐧 作者QQ：168847242 | 📩 邮箱：168847242@qq.com</p>

### 📢 项目介绍

```
这是一个使用 Vue3 开发的音乐网站项目
实现比较简单，功能数量较少，适合小白、初学者学习参考
该项目没有制作登录功能，只是实现了一个简单的音乐网站
因此，会员歌曲只有 30秒 的试听！
```

- 本项目后端所有数据来自开源项目 NeteaseCloudMusicApi
- <a href="https://github.com/Binaryify/NeteaseCloudMusicApi" target="_blank">项目仓库地址</a>
- <a href="https://binaryify.github.io/NeteaseCloudMusicApi/#/" target="_blank">API 文档地址</a>

### 🛠️ 功能列表

1. 歌曲收藏（本地存储形式）
2. 播放列表
3. 播放歌单歌曲
4. 搜索建议
5. 歌词滚动
6. 音频可视化
7. 可以自行下载项目查看 🍓

### 🌐 更新内容：

暂无

### 🥠 运行项目

#### 安装依赖

```
npm install
或
npm i
```

#### 运行项目

```
npm run dev
```

#### 项目构建

```
npm run build
```

### 🧩 项目效果预览

在线预览地址：<a href="https://music.hapyit.com" target="_blank">LYMusic</a>

1. 首页

   <img src="./doc/lymusic_home.png" align="center">

2. 搜索

   <img src="./doc/lymusic_searchResult.png" align="center">

3. 播放音乐

   <img src="./doc/lymusic_playMusic.png" align="center">
