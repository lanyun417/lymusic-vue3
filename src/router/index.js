import { createRouter, createWebHashHistory } from 'vue-router';

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      // 首页
      path: '/',
      name: 'Home',
      component: () => import('@/views/Home.vue'),
      meta: {
        title: '首页',
      },
    },
    {
      // 歌单详情
      path: '/songListDetail/:id',
      name: 'SongListDetail',
      component: () => import('@/views/SonglistDetail.vue'),
      meta: {
        title: '歌单详情',
      },
    },
    {
      // 全部歌单
      path: '/allSonglist',
      name: 'AllSonglist',
      component: () => import('@/views/AllSonglist.vue'),
      meta: {
        title: '全部歌单',
      },
    },
    {
      // 歌曲榜单
      path: '/songTopList',
      name: 'SongTopList',
      component: () => import('@/views/SongTopList.vue'),
      meta: {
        title: '歌曲榜单',
      },
    },
    {
      name: 'Search',
      path: '/search',
      component: () => import('@/views/Search.vue'),
      meta: {
        title: '搜索',
      },
    },
  ],
  scrollBehavior() {
    return {
      left: 0,
      top: 0,
    };
  },
});

router.beforeEach((to, from, next) => {
  let title = 'LYMusic';
  if (to.meta.title === '首页') {
    document.title = title;
  } else {
    document.title = `${title} - ${to.meta.title}`;
  }
  next();
});

export default router;
