import { defineStore } from 'pinia';
import { ElMessage } from 'element-plus';

export default defineStore('playOrderStore', {
  state: () => {
    return {
      currentOrder: '1', // 1 -> 顺序播放、2 -> 随机播放、 3 -> 单曲循环
    };
  },
  actions: {
    changePlayOrder() {
      if (this.currentOrder === '1') {
        this.currentOrder = '2';
        ElMessage({
          type: 'success',
          message: '随机播放',
        });
        return;
      }
      if (this.currentOrder === '2') {
        this.currentOrder = '3';
        ElMessage({
          type: 'success',
          message: '单曲循环',
        });
        return;
      }
      if (this.currentOrder === '3') {
        this.currentOrder = '1';
        ElMessage({
          type: 'success',
          message: '顺序播放',
        });
        return;
      }
    },
  },
});
