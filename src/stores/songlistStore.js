import { defineStore } from 'pinia';

// 歌单
export default defineStore('songlistStore', {
  state: () => {
    return {
      // 当前歌单分类
      currentSonglistCategory: '全部',
    };
  },
  actions: {
    // 改变歌单分类（全部歌单页面使用）
    changeCategory(val) {
      this.currentSonglistCategory = val;
    },
  },
});
