import { defineStore } from 'pinia';
import { ElMessage } from 'element-plus';

// 播放列表
export default defineStore('playlistStore', {
  state: () => {
    return {
      // 歌单歌曲列表
      songlistSongs: [],
      // 播放列表（手动添加到播放列表的歌曲）
      playlist: [],
      // 正在播放的歌曲列表
      currentPlaylist: [],
      showDrawer: false,
      // 当前歌曲信息
      newSongInfo: {
        id: null,
        cover:
          'https://myfiles-lanyun.oss-cn-beijing.aliyuncs.com/lymusic-logo', // 歌曲封面
        name: '歌曲名称', // 歌曲名称
        singers: [
          {
            id: 0,
            name: '——',
          },
        ], // 歌手信息
        album: '——', // 歌曲专辑
        lyric: [], // 歌词
        url: null, // 音频资源
        duration: '00:00', // 歌曲持续时间
      },
    };
  },
  actions: {
    /**
     * 添加歌曲到播放列表 playlist
     * @param { number } song 歌曲
     * @param { 'del' | 'add'  } type 操作类型
     */
    addSongToPlaylist(song, type = 'add') {
      const index = this.playlist.findIndex((item) => item.id === song.id);

      if (index !== -1) {
        // 如果歌曲已存在于播放列表，将其从原位置删除
        this.playlist.splice(index, 1);
      }

      // 删除
      if (type === 'del') {
        ElMessage({
          message: '歌曲已从播放列表中移除',
          type: 'warning',
        });
        return;
      }

      // 添加
      if (type === 'add') {
        // 将歌曲添加到列表末尾
        this.playlist.push(song);
        ElMessage({
          message: '添加成功',
          type: 'success',
        });
      }
    },
    // 修改播放歌单中的歌曲
    changeSonglistSongs(newArray) {
      // 如果有数据则清空
      if (this.songlistSongs.length > 0) {
        this.songlistSongs.splice(0);
      }
      if (newArray instanceof Array) {
        newArray.forEach((item) => {
          this.songlistSongs.push(item);
        });
      }
    },
    // 切换正在播放的播放列表（默认为手动添加的播放列表）
    switchCurrentPlaylist(list) {
      this.currentPlaylist = list;
      console.log(this.currentPlaylist);
    },
  },
});
