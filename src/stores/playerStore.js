import { defineStore } from 'pinia';
import { simiSong } from '@/api/song';

// 播放器
export default defineStore('playerStore', {
	state: () => {
		return {
			// 当前歌曲信息
			currentSongInfo: {
				id: null,
				cover: 'https://lanyun-files.oss-cn-beijing.aliyuncs.com/logo.png', // 歌曲封面
				name: '歌曲名称', // 歌曲名称
				singers: [
					{
						id: 0,
						name: '——',
					},
				], // 歌手信息
				album: '——', // 歌曲专辑
				lyric: [], // 歌词
				url: null, // 音频资源
				duration: '00:00', // 歌曲持续时间
			},
			// 播放器播放状态
			playStatus: false,
			simiSongList: [],
		};
	},
	actions: {
		// 修改播放状态
		changePlayStatus(val) {
			this.playStatus = val;
		},
		// 切换歌曲
		changeSong(song) {
			this.currentSongInfo = Object.assign({}, song);
		},
		// 获取相似歌曲
		async getSimilarSong() {
			const res = (await simiSong(this.currentSongInfo.id)).data;
			this.simiSongList = res.songs;
			console.log(this.simiSongList);
		},
	},
});
