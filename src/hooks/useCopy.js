import ClipboardJS from 'clipboard';
import { ElMessage } from 'element-plus';

export function handleCopy(e, text) {
  const clipboard = new ClipboardJS(e.target, { text: () => text });
  clipboard.on('success', (e) => {
    ElMessage({ type: 'success', message: '复制成功' });
    // 释放内存
    clipboard.off('error');
    clipboard.off('success');
    clipboard.destroy();
  });
  clipboard.on('error', (e) => {
    // 不支持复制
    ElMessage({ type: 'waning', message: '复制失败' });
    // 释放内存
    clipboard.off('error');
    clipboard.off('success');
    clipboard.destroy();
  });
  clipboard.onClick(e);
}
