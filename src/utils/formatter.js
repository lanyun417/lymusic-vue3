/**
 * 格式化数值为 10万，100万...
 * @param { number } number
 * @returns { string }
 */
export function formatNumberWithUnit(number) {
  const units = ['', '万', '亿', '万亿'];
  let unitIndex = 0;
  let result = number;
  while (result >= 10000 && unitIndex < units.length - 1) {
    result /= 10000;
    unitIndex++;
  }
  result = Math.floor(result * 100) / 100; // 保留两位小数
  return result.toString() + units[unitIndex];
}

/**
 * 格式化小于 10 的数字前追加 0
 * @param { number } num
 * @returns { string }
 */
export function formatNumber(num) {
  return num < 10 ? '0' + num : num.toString();
}

/**
 * 格式化 audio 音频当前的播放时间
 * @param { * } audioElement
 * @returns { string }
 */
export function formatAudioTime(time) {
  if (time === '00:00') return time;
  // 将时间转换为分钟和秒钟
  let minutes = Math.floor(time / 60);
  let seconds = Math.floor(time % 60);
  // 格式化分钟和秒钟，确保它们始终有两位数
  let formattedTime = `${minutes.toString().padStart(2, '0')}:${seconds
    .toString()
    .padStart(2, '0')}`;
  if (formattedTime === 'NaN:NaN') {
    return '00:00';
  }
  return formattedTime;
}

/**
 * 格式化 audio 的当前时间与歌词时间格式对应
 * @param { number } time
 * @returns { string }
 */
export function formatLyricTime(lyricTime) {
  // 格式化歌词的时间 转换成 sss:ms
  const timeParts = lyricTime.split(':');
  const minutes = parseInt(timeParts[0]);
  const seconds = parseFloat(timeParts[1]);
  const formattedTime = minutes * 60 + seconds;
  return formattedTime;
}

/**
 * 毫秒转分钟
 * @param { number } milliseconds 毫秒
 * @returns
 */
export function formatMilliseconds(milliseconds) {
  const totalSeconds = Math.floor(milliseconds / 1000);
  const minutes = Math.floor(totalSeconds / 60);
  const seconds = totalSeconds % 60;

  const formattedMinutes = String(minutes).padStart(2, '0');
  const formattedSeconds = String(seconds).padStart(2, '0');

  return `${formattedMinutes}:${formattedSeconds}`;
}
