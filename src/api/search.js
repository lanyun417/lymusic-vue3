import request from '@/utils/request';

/**
 * 获取搜索建议
 * @param { string } keyword 关键词
 * @returns
 */
export function searchSuggest(keyword) {
  return request.post(`/search/suggest?keywords=${keyword}`);
}

/**
 * 获取搜索结果
 * @param { string } keyword 搜索关键字
 * @param { number } limit 返回结果条数
 * @param { number } offset 返回结果偏移量
 * @returns
 */
export function searchResult(keyword, limit = 50, offset = 0) {
  return request.post(
    `/search?keywords=${keyword}&limit=${limit}&offset=${offset}`
  );
}
