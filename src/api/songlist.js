import request from '@/utils/request';

/**
 * 获取热门歌单分类
 * @returns { object }
 */
export function hotSonglistTag() {
  return request.post('/playlist/hot');
}

/**
 * 获取推荐歌单
 * @param { number } limit 获取数量
 * @returns
 */
export function getRecSonglist(limit) {
  return request.post(`/personalized?limit=${limit}`);
}

/**
 * 获取歌单详情
 * @param { number } id 歌单ID
 */
export function songlistDetail(id) {
  return request.post(`/playlist/detail?id=${id}`);
}

/**
 * 获取所有歌单分类
 * @returns
 */
export function allSonglistCategory() {
  return request.post('/playlist/catlist');
}

/**
 * 获取分类歌单
 * @param { number } limit 获取数量
 * @param { number } offset 偏移量
 * @param { string } name 分类名称
 * @returns
 */
export function songlistByCategory(limit = 50, offset = 0, name = false) {
  const _name = encodeURI(name);
  if (!name) {
    console.log('name is not found');
    return request.post(
      `/top/playlist?cat=全部&limit=${limit}&offset=${offset}`
    );
  } else {
    return request.post(
      `/top/playlist?cat=${_name}&limit=${limit}&offset=${offset}`
    );
  }
}
