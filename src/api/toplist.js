import request from '@/utils/request';

/**
 * 获取所有榜单
 * @returns
 */
export function allToplist() {
  return request.post('/toplist');
}
