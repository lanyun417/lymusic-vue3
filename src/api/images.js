import request from '@/utils/request';

/**
 * 获取轮播图
 * @param { string } type 图片类型
 * 0 - PC
 * 1 - Android
 * 2 - iphone
 * 3 - ipad
 * @exmple
 */
export function getBanner(type) {
  return request.post(`/banner?type=${type}`);
}
