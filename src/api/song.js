import request from '@/utils/request';

/**
 * 获取新歌推荐
 * @param { number } type 0 全部 | 7 华语 | 96 欧美 | 8 日本 | 16 韩国
 */
export function newSongRec(type) {
  return request.post(`/top/song?type=${type}`);
}

/**
 * 获取歌单所有歌曲
 * @param { number } id
 * @param { number } limit
 * @param { number } offset
 * @returns { object }
 */
export function playlistSong(id, limit, offset) {
  return request.post(
    `/playlist/track/all?id=${id}&limit=${limit}&offset=${offset}`
  );
}

/**
 * 获取歌曲详情
 * @param { number } id 歌曲ID
 */
export function songDetail(id) {
  return request.post(`/song/detail?ids=${id}`);
}

/**
 * 获取歌曲URL
 * @param { number } id 歌曲ID
 * @param { string } level 音质等级
 * standard => 标准,higher => 较高, exhigh=>极高, lossless=>无损, hires=>Hi-Res
 * jyeffect => 高清环绕声, sky => 沉浸环绕声, jymaster => 超清母带
 */
export function songUrl(id, level = 'higher') {
  return request.post(`/song/url?id=${id}&level=${level}`);
}

/**
 * 获取歌曲歌词
 * @param { number } id 歌曲ID
 * @returns
 */
export function lyric(id) {
  return request.post(`/lyric?id=${id}`);
}

/**
 * 获取相似歌曲
 * @param { number } id 歌曲ID
 * @returns
 */
export function simiSong(id) {
  return request.post(`/simi/song?id=${id}`);
}
