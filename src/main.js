import { createApp } from 'vue';
import { createPinia } from 'pinia';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
import App from './App.vue';
import router from './router';
import '@/assets/css/normalize.css';
import lazyPlugin from 'vue3-lazy';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';
import _ from 'lodash';

const app = createApp(App);

// lodash
app.config.globalProperties.lodash = _;

app.use(createPinia());
app.use(router);
app.use(ElementPlus);
app.use(lazyPlugin, {
  loading: 'https://myfiles-lanyun.oss-cn-beijing.aliyuncs.com/lymusic-logo',
  error: 'https://myfiles-lanyun.oss-cn-beijing.aliyuncs.com/lymusic-logo',
});
// 引入所有 element-plus/icons
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}
app.mount('#app');
